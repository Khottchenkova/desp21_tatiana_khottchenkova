--Create database that monitors workload, capabilities and activities of our city's health institutions. 
---The database needs to represent institutions, their locations, staffing, capacity, capabilities and patients' visits.
--Constraints:
--6+ tables
--5+ rows in every table, 50+ rows total
--3NF, Primary and Foreign keys must be defined
--Not null constraints where appropriate and at least 2 check constraints of other type
--Using DEFAULT and GENERATED ALWAYS AS are encouraged
--2. Write a query to identify doctors with insufficient workload 
--(less than 5 patients a month for the past few months)
--Present the result as a single sqlfile (name of the sqlfile should be SQL_DDL_Name_Surname_Task)


--DataBase represents helth institution with main office, children's clinic and 3 branch offices. Medcine staff
--can work in different branche offices. 
CREATE DATABASE health_institutions;
CREATE SCHEMA health_institutions;
SET search_path TO health_institutions;
--1
--this table contains data on addresses of helth institutions and patients
CREATE TABLE address (
						address_id serial NOT NULL PRIMARY KEY,
						address TEXT NOT NULL UNIQUE);
--2	
--This table contains helth institutions and connectet to its addresses				
CREATE TABLE organization(
						organization_id serial NOT NULL PRIMARY KEY,
						organization TEXT NOT NULL UNIQUE,
						address_id integer NOT NULL REFERENCES address(address_id)
						);
--3
--This table contains all posible medcine specialities represented in the helth care chain and prices for one visit
--for each speciality				
CREATE TABLE speciality (
						speciality_id serial NOT NULL PRIMARY KEY,
						speciality text NOT NULL UNIQUE,
						capacity integer NOT NULL, -- maximum number OF patients per MONTH FOR speciality
						price money NOT NULL  --price FOR one visit 
					);
--4
--This table contains information about patients and connected to the table with addresses				
CREATE TABLE patient (
					patient_id serial NOT NULL PRIMARY KEY,
					first_name varchar(200) NOT NULL,
					last_name varchar(200) NOT NULL,
					full_name varchar(400) GENERATED ALWAYS AS (first_name||' '||last_name) STORED NOT NULL,
					address_id integer NOT NULL REFERENCES address(address_id), 
					date_of_birth date NOT NULL CHECK (date_of_birth BETWEEN current_date - INTERVAL '120 year' AND current_date )
					);
--5
--This table contains information on medcine staff
CREATE TABLE doctor (
					doctor_id serial NOT NULL PRIMARY KEY,
					first_name varchar(200) NOT NULL,
					last_name varchar(200) NOT NULL,
					full_name varchar(400) GENERATED ALWAYS AS (first_name||' '||last_name) STORED NOT NULL,
					speciality_id integer NOT NULL REFERENCES speciality (speciality_id)
					);
--6	
--This table contains information about which doctors work in which company divisions with which workload. 			
CREATE TABLE workload (
						organization_id integer NOT NULL REFERENCES organization (organization_id),
						doctor_id integer NOT NULL  REFERENCES doctor (doctor_id),
						capacity integer NOT NULL,  --maximum number OF patients per MONTH FOR current doctor IN current organization. for example: 6 hours 20 workdays, 15 minutes for patient = 480  
						CONSTRAINT pk_wl PRIMARY KEY (organization_id, doctor_id)
					);
--7
--This table contains information about patient visits to doctors
CREATE TABLE visit (
					visit_id serial NOT NULL PRIMARY KEY,
					patient_id integer NOT NULL REFERENCES patient(patient_id),
					doctor_id integer NOT NULL REFERENCES doctor (doctor_id),
					organization_id integer NOT NULL REFERENCES organization (organization_id),
					visit_date date NOT NULL DEFAULT current_date CHECK (visit_date <= current_date)
					);
--1:
INSERT INTO address (address)
VALUES ('Nezavisimosti ave., 58'),
		('Gikalo st., 1'),
		('Prityckogo st., 140'),
		('Pobediteley ave., 133'),
		('Surganova st., 17'),
		('Pulikhova st., 32-2'),
		('Shugaeva st., 23-32'),
		('Plekhanova st., 63-7'),
		('Orlovskaya st., 24-54'),
		('Odoevskogo st., 134-158')
RETURNING *;
--2:
INSERT INTO organization (organization, address_id)
VALUES ('Head office', (SELECT a.address_id
						FROM address a
						WHERE address = 'Nezavisimosti ave., 58')),
		('Children''s medical center', (SELECT a.address_id
						FROM address a
						WHERE address = 'Gikalo st., 1')),
		('Branch Office #1', (SELECT a.address_id
						FROM address a
						WHERE address = 'Prityckogo st., 140')),
		('Branch Office #2', (SELECT a.address_id
						FROM address a
						WHERE address = 'Pobediteley ave., 133')),
		('Branch Office #3', (SELECT a.address_id
						FROM address a
						WHERE address = 'Surganova st., 17'))
RETURNING *;
--3			
INSERT INTO speciality (speciality, capacity, price)
VALUES ('Therapist', 480, 35),
		('Pediatrician', 480, 35),
		('Neurologist', 360, 38),
		('Otorhinolaryngologist', 300, 43),
		('Ophthalmologist', 300, 38)
RETURNING *;
--4
INSERT INTO patient (first_name, last_name, address_id, date_of_birth)
VALUES ('Ivan', 'Sakharov', (SELECT a.address_id
						FROM address a
						WHERE address = 'Pulikhova st., 32-2'), '1986-09-01'::date),
		('Polina', 'Sakharova', (SELECT a.address_id
						FROM address a
						WHERE address = 'Pulikhova st., 32-2'), '2012-10-01'::date),
		('Sofia', 'Sokolova', (SELECT a.address_id
						FROM address a
						WHERE address = 'Shugaeva st., 23-32'), '1954-08-09'::date),
		('Andrey', 'Stepanov', (SELECT a.address_id
						FROM address a
						WHERE address = 'Plekhanova st., 63-7'), '1979-02-11'::date),
		('Anna', 'Petrova', (SELECT a.address_id
						FROM address a
						WHERE address = 'Orlovskaya st., 24-54'), '1962-04-26'::date),
		('Larisa', 'Kurdjumova', (SELECT a.address_id
						FROM address a
						WHERE address = 'Odoevskogo st., 134-158'), '2016-03-07'::date), 
		('Marina', 'Kurdjumova', (SELECT a.address_id
						FROM address a
						WHERE address = 'Odoevskogo st., 134-158'), '1992-08-01'::date),
		('Pavel', 'Kurdjumov', (SELECT a.address_id
						FROM address a
						WHERE address = 'Odoevskogo st., 134-158'), '1990-06-21'::date),
		('Ivan', 'Kurdjumov', (SELECT a.address_id
						FROM address a
						WHERE address = 'Odoevskogo st., 134-158'), '2020-09-11'::date)
RETURNING *;

INSERT INTO doctor (first_name, last_name, speciality_id)
VALUES ('Anna','Kulikova', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Therapist')),
		('Evgeny','Starovojtov', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Therapist')),
		('Olga','Gladun', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Pediatrician')),
		('Anjhela','Lukashejko', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Pediatrician')),
		('Nadezhda','Shevchenko', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Neurologist')),
		('Marina','Solovej', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Otorhinolaryngologist')),
		('Sergej','Streltsov', (SELECT s.speciality_id 
						FROM speciality s
						WHERE s.speciality = 'Ophthalmologist'))
	RETURNING *;
--6
INSERT INTO workload (organization_id, doctor_id, capacity)
VALUES ((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Head office'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
																				240),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Head office'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
																				200),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Head office'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
																				200),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Head office'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
																				100),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Children''s medical center'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
																				160),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Children''s medical center'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Olga' AND d.last_name = 'Gladun'),
																				240),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Children''s medical center'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
																				480),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Branch Office #1'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
																				240),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Branch Office #1'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
																				100),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Branch Office #2'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
																				240),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Branch Office #2'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
																				200),
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Branch Office #3'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Olga' AND d.last_name = 'Gladun'),
																				240),																		
		((SELECT o.organization_id
		FROM organization o
		WHERE o.organization ='Branch Office #3'),
									(SELECT d.doctor_id
									FROM doctor d
									WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
																				240)
		RETURNING *;
--7
INSERT INTO visit (patient_id, doctor_id, organization_id, visit_date)
VALUES ((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-02'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-04-02'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-02'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-04'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-06'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-16'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Marina'  AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-26'::date),													
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-25'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-04-08'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-16'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-09'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-15'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan'  AND p.last_name = 'Sakharov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-25'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia'  AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia'  AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-08'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia'  AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-09'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia'  AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-10'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia'  AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-11'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia'  AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia' AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-04-03'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Andrey' AND p.last_name = 'Stepanov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-04-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Anna' AND p.last_name = 'Petrova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-03'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Anna' AND p.last_name = 'Petrova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-04-07'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Anna' AND p.last_name = 'Petrova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-03-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Anna' AND p.last_name = 'Petrova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Head office'),
															'2021-02-17'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-02'::date),
			((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-03-02'::date),
			((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-05'::date),
			((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-11'::date),
			((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-15'::date),
			((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-02'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-02-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-02-15'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Larisa' AND p.last_name = 'Kurdjumova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anjhela' AND d.last_name = 'Lukashejko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-02-17'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-02-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Polina' AND p.last_name = 'Sakharova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-03-11'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-03-15'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-03-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Nadezhda' AND d.last_name = 'Shevchenko'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Children''s medical center'),
															'2021-04-02'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Andrey' AND p.last_name = 'Stepanov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #2'),
															'2021-04-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Andrey' AND p.last_name = 'Stepanov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Marina' AND d.last_name = 'Solovej'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #1'),
															'2021-03-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Andrey' AND p.last_name = 'Stepanov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Anna' AND d.last_name = 'Kulikova'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #1'),
															'2021-04-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia' AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #2'),
															'2021-04-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia' AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #2'),
															'2021-03-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia' AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #2'),
															'2021-02-22'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Sofia' AND p.last_name = 'Sokolova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #2'),
															'2021-02-12'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Anna' AND p.last_name = 'Petrova'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Sergej' AND d.last_name = 'Streltsov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #2'),
															'2021-04-22'::date),													
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Olga' AND d.last_name = 'Gladun'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #3'),
															'2021-03-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Olga' AND d.last_name = 'Gladun'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #3'),
															'2021-03-07'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Olga' AND d.last_name = 'Gladun'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #3'),
															'2021-04-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Ivan' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Olga' AND d.last_name = 'Gladun'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #3'),
															'2021-02-05'::date),
		((SELECT p.patient_id
			FROM patient p
			WHERE p.first_name = 'Pavel' AND p.last_name = 'Kurdjumov'), 
								(SELECT d.doctor_id
								FROM doctor d
								WHERE d.first_name = 'Evgeny' AND d.last_name = 'Starovojtov'),
											(SELECT o.organization_id
											FROM organization o
											WHERE o.organization = 'Branch Office #3'),
															'2021-03-05'::date)													

RETURNING *;

--2. Write a query to identify doctors with insufficient workload 
--(less than 5 patients a month for the past few months)
--Less than 5 visits per month for each of 3 last monthes (the current month is not taken into account):
SELECT d.full_name doctor_name, s.speciality  
FROM doctor d 
JOIN speciality s 
ON d.speciality_id = s.speciality_id 
WHERE d.doctor_id IN
(SELECT v.doctor_id
						FROM visit v 
						WHERE v.visit_date between (SELECT date_trunc('MONTH',now())::DATE - INTERVAL '1 month') AND (SELECT date_trunc('MONTH',now())::DATE)
						GROUP BY doctor_id
						HAVING count(v.visit_id) <5)
AND d.doctor_id IN
(SELECT v.doctor_id
						FROM visit v 
						WHERE v.visit_date between (SELECT date_trunc('MONTH',now())::DATE - INTERVAL '2 month') AND (SELECT date_trunc('MONTH',now())::DATE- INTERVAL '1 month')
						GROUP BY doctor_id
						HAVING count(v.visit_id) <5)
AND d.doctor_id IN
(SELECT v.doctor_id
						FROM visit v 
						WHERE v.visit_date between (SELECT date_trunc('MONTH',now())::DATE - INTERVAL '3 month') AND (SELECT date_trunc('MONTH',now())::DATE- INTERVAL '2 month')
						GROUP BY doctor_id
						HAVING count(v.visit_id) <5)
						;