--Choose your top-3 favorite movies and add them to 'film' table. 
--Fill rental rates with 4.99, 9.99 and 19.99 and rental durationswith 1, 2 and 3 weeksrespectively.

--film table structure: film_id (default), title, description, release_year, language_id (FK language), original_language_id (FK language), 
--rental_duration (DEFAULT), rental_rate (default), length, replacement_cost (default), rating (enum datatype), last_update (default), 
--special_features, fulltext(default)
INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, rating, special_features)--text datatype)
-- English - language_id = 1, German - language_id = 6, French - language_id = 5
VALUES 
--Bridget Jones's Diary - 2001 year - 97 min - english
(UPPER('Bridget Jones''s Diary'), 
'A 32-year-old English single woman, who writes a diary which focuses on the things she wishes to happen in her life.', 
2001, 
(SELECT l.language_id FROM "language" l WHERE upper(l."name")='ENGLISH'),
(SELECT l.language_id FROM "language" l WHERE upper(l."name")='ENGLISH'),
7,
4.99,
97,
'R'::mpaa_rating, 
'{Trailers,Behind the Scenes}'), 

--The Intouchables - 2011 year - 112 min - french
(UPPER ('The Intouchables'),
'After he becomes a quadriplegic from a paragliding accident, an aristocrat hires a young man from the projects to be his caregiver.',
2011,
(SELECT l.language_id FROM "language" l WHERE upper(l."name")='ENGLISH'),
(SELECT l.language_id FROM "language" l WHERE upper(l."name")='FRENCH'),
14,
9.99,
112,
'R'::mpaa_rating,
'{Commentaries,Behind the Scenes}' ),

--Knockin' on Heaven's Door - 1997 year - 87 min - german
(upper ('Knockin'' on Heaven''s Door'),
'Two terminally ill men meet, steal a car and fulfill one''s dream of going to the sea.',
1997,
(SELECT l.language_id FROM "language" l WHERE upper(l."name")='ENGLISH'),
(SELECT l.language_id FROM "language" l WHERE upper(l."name")='GERMAN'),
21,
19.99,
87,
'R'::mpaa_rating,
'{Trailers,Deleted Scenes,Behind the Scenes}')
RETURNING *;


--Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total).

--Bridget Jones's Diary: Ren?e Zellweger, Colin Firth, Hugh Grant
--The Intouchables: Fran?ois Cluzet, Omar Sy
--Knockin' on Heaven's Door: Til Schweiger, Jan Josef Liefers
--actor table structure: actor_id (default), first_name, last_name, last_update (default)
INSERT INTO actor (first_name, last_name)
VALUES
(upper('Ren?e'), upper('Zellweger')),
(upper('Colin'), upper('Firth')),
(upper('Hugh'), upper('Grant')),
(upper('Fran?ois'), upper('Cluzet')),
(upper('Omar'), upper('Sy')),
(upper('Til'), upper('Schweiger')),
(upper('Jan Josef'), upper('Liefers'))
RETURNING *; 

--Bridget Jones's Diary: Ren?e Zellweger, Colin Firth, Hugh Grant
--The Intouchables: Fran?ois Cluzet, Omar Sy
--Knockin' on Heaven's Door: Til Schweiger, Jan Josef Liefers
--film_actor table structure: actor_id (FK actor), film_id(FK film), last_update (default)
INSERT INTO film_actor (actor_id, film_id)
VALUES 
((SELECT a.actor_id
					FROM actor a 
					WHERE upper (a.first_name) = upper('Ren?e')
					AND upper(a.last_name) = upper('Zellweger')), 
																	(SELECT f2.film_id
																			FROM film f2
																			WHERE upper(f2.title) = UPPER('Bridget Jones''s Diary')
																	)),
((SELECT a.actor_id
		FROM actor a
		WHERE upper (a.first_name) = upper('Colin')
		AND upper(a.last_name) = upper('Firth')
	), 
																(SELECT f2.film_id
																		FROM film f2
																		WHERE upper(f2.title) = UPPER('Bridget Jones''s Diary')
																)),
((SELECT a.actor_id
		FROM actor a
		WHERE upper (a.first_name) = upper('Hugh')
		AND upper(a.last_name) = upper('Grant')
					), 
															(SELECT f2.film_id
																	FROM film f2
																	WHERE upper(f2.title) = UPPER('Bridget Jones''s Diary')
															)),
((SELECT a.actor_id
		FROM actor a 
		WHERE upper (a.first_name) = upper('Fran?ois')
		AND upper(a.last_name) = upper('Cluzet')
					),
																(SELECT f2.film_id
																		FROM film f2
																		WHERE upper(f2.title) = upper('The Intouchables')
																)),
((SELECT a.actor_id
		FROM actor a 
		WHERE upper (a.first_name) = upper('Omar')
		AND upper(a.last_name) =  upper('Sy')
		),
																(SELECT f2.film_id
																		FROM film f2
																		WHERE upper(f2.title) = upper('The Intouchables')
																)),
((SELECT a.actor_id
		FROM actor a 
		WHERE upper (a.first_name) = upper('Til')
		AND upper(a.last_name) = upper('Schweiger')
	),
																(SELECT f2.film_id
																		FROM film f2
																		WHERE  upper(f2.title) = upper('Knockin'' on Heaven''s Door')
																)),
((SELECT a.actor_id
		FROM actor a 
		WHERE upper (a.first_name) =  upper('Jan Josef')
		AND upper(a.last_name) = upper('Liefers')
					),
																(SELECT f2.film_id
																		FROM film f2
																		WHERE  upper(f2.title) = upper('Knockin'' on Heaven''s Door')
																))
					
					RETURNING *;


--Add your favorite movies to any store's inventory.
--inventory table structure: inventory_id (default), film_id (FK film), store_id (FK store), last_update (default)
INSERT INTO inventory (film_id, store_id)
VALUES
((SELECT f2.film_id
		FROM film f2
		WHERE upper(f2.title) = UPPER('Bridget Jones''s Diary')),
																		(SELECT s.store_id 
																				FROM store s
																				ORDER BY random() LIMIT 1)),
((SELECT f2.film_id
		FROM film f2
		WHERE upper(f2.title) = UPPER('The Intouchables')),
																		(SELECT s.store_id 
																				FROM store s
																				ORDER BY random() LIMIT 1)),
((SELECT f2.film_id
		FROM film f2
		WHERE upper(f2.title) = UPPER('Knockin'' on Heaven''s Door')),
																		(SELECT s.store_id 
																				FROM store s
																				ORDER BY random() LIMIT 1))
RETURNING *;

--adding corresponding data to film_category table -additional problem
--film_category table structure: film_id (FK film), category_id (FK category), last_update(default)
INSERT INTO film_category (film_id, category_id)
VALUES
((SELECT f.film_id
		FROM film f
		WHERE UPPER(f.title) = UPPER('Bridget Jones''s Diary')),
																(SELECT c.category_id
																		FROM category c
																		WHERE upper(c."name")='COMEDY')),
((SELECT f.film_id
		FROM film f
		WHERE UPPER(f.title) = UPPER ('The Intouchables')),
																(SELECT c.category_id
																		FROM category c
																		WHERE upper(c."name")='COMEDY')),
((SELECT f.film_id
		FROM film f
		WHERE UPPER(f.title) = UPPER('Knockin'' on Heaven''s Door')),
																	(SELECT c.category_id
																			FROM category c
																			WHERE upper(c."name")='DRAMA'))
RETURNING *;

--Alter any existing customer in the database who has at least 43 rental and 43 payment records. 
--Change his/her personal data to yours (first name, last name, address, etc.).
--Do not perform any updates on 'address' table, as it can impact multiple records with the same address. 
--Change customer'screate_datevalue to current_date.

--According to the condition of the problem, any customer that meets the given condition could be chosen. 
--Therefore, a random customer that meets the given condition is selected.
--customer table structure: customer_id (default), store_id , first_name, last_name, email, address_id, activebool (default), create_date (default), 
--last_update (default), active
UPDATE customer c 
SET first_name = 'TATIANA',
	last_name ='KHOTTCHENKOVA', 
	email = 'KHOTTCHENKOVA@gmail.com',
	create_date = current_date
	WHERE c.customer_id IN (
						(SELECT r.customer_id			--selecting castumers who has at least 43 rental records
								FROM rental r
								GROUP BY r.customer_id
								HAVING count(r.rental_id) >= 43) 
			INTERSECT 
						(SELECT p.customer_id			--selecting one random customer who has at least 43 payment records. 
								FROM payment p 					--I'v checked the existance OF EACH customer_id of the SECOND SELECT IN the FIRST SELECT with the using of EXCEPT 
								GROUP BY p.customer_id
								HAVING count(p.rental_id) >= 43
								ORDER BY random() LIMIT 1
						))
	RETURNING *; 

--Remove any records related to you (as a customer) from all tables except 'Customer'and 'Inventory'
				
--payment_p2017_01 and others have FK to rental table
DELETE 
--SELECT * --for easier checking
FROM 
payment p 
WHERE p.customer_id IN (
					SELECT customer.customer_id 
							FROM customer 
							WHERE upper(customer.last_name) = 'KHOTTCHENKOVA'
)
RETURNING *;

DELETE 
--SELECT *
FROM 
rental 
WHERE rental.customer_id IN (
								SELECT customer.customer_id 
										FROM customer 
										WHERE upper(customer.last_name) = 'KHOTTCHENKOVA'
)
RETURNING *;

--Rent you favorite movies from the store they are in and pay for them 
--(add corresponding records to the database to represent this activity).

--rental and payment tables are needed

--We need to create new partition for adding payment record with current date
CREATE TABLE public.payment_p2021_05 PARTITION OF public.payment FOR VALUES FROM ('2021-05-01 00:00:00+03') TO ('2021-06-01 00:00:00+03');
--The analysis of the existing data in the DataBase justify that each staff member could work in both rental stores, so it could be any staff_id there.
--rental table structure: rental_id (default), rental_date, inventory_id (FK invenory), customer_id (FK customer), return_date, staff_id (FK staff), 
--last_update (default)
INSERT INTO rental (rental_date, inventory_id, customer_id, staff_id)
VALUES (current_date,
--inventory_id 
(SELECT inventory_id 																--selecting OF inventory_id OF my favorite movie
		FROM inventory 
		WHERE EXISTS (SELECT film.film_id 
							FROM film
							WHERE upper(film.title) ='KNOCKIN'' ON HEAVEN''S DOOR' 
							AND inventory.film_id=film.film_id)),
--customer_id 
(SELECT customer.customer_id														--selecting of my customer_id 
		FROM customer
		WHERE upper(customer.last_name)='KHOTTCHENKOVA'),
--staff_id 
(SELECT staff.staff_id 
		FROM staff 
		ORDER BY random() 
		LIMIT 1)),						-- random staff member
(current_date,
--inventory_id 
(SELECT inventory_id 
		FROM inventory 
		WHERE EXISTS (SELECT film.film_id 
							FROM film
							WHERE upper(film.title) = 'BRIDGET JONES''S DIARY'
							AND inventory.film_id=film.film_id)),
--customer_id 
(SELECT customer.customer_id 
		FROM customer
		WHERE upper(customer.last_name)='KHOTTCHENKOVA'),
--staff_id 
(SELECT staff.staff_id 
		FROM staff 
		ORDER BY random() 
		LIMIT 1)),
(current_date,
--inventory_id 
(SELECT inventory_id 
		FROM inventory 
		WHERE EXISTS (SELECT film.film_id 
							FROM film
							WHERE upper(film.title) = 'THE INTOUCHABLES'
							AND inventory.film_id=film.film_id)),
--customer_id 
(SELECT customer.customer_id 
		FROM customer
		WHERE upper(customer.last_name)='KHOTTCHENKOVA'),
--staff_id 
(SELECT staff.staff_id 
		FROM staff 
		ORDER BY random() 
		LIMIT 1))
RETURNING *;

--payment table structure: payment_id (default), customer_id, staff_id, rental_id, amount, payment_date
--I connected rental_id with payment_id in assumtion that I'v rented and paid by myself. 
--Actually, there are a few records in the DataBase from which it follows that one customer rented, and another paid for it.
INSERT INTO payment (customer_id, staff_id, rental_id, amount, payment_date)
VALUES 
--customer_id 
((SELECT customer.customer_id 
		FROM customer
		WHERE upper(customer.last_name)='KHOTTCHENKOVA'),
--staff_id 
(SELECT staff.staff_id 
		FROM staff 
		ORDER BY random() 
		LIMIT 1),
--rental_id
(SELECT rental.rental_id 
		FROM rental 
		INNER JOIN inventory 
		ON rental.inventory_id = inventory.inventory_id 
		WHERE EXISTS (SELECT film.film_id 
							FROM film
							WHERE upper(film.title) = 'BRIDGET JONES''S DIARY'
							AND inventory.film_id=film.film_id)), 
--amount
4.99,
--payment_date
now()),
--customer_id 
((SELECT customer.customer_id 
		FROM customer
		WHERE upper(customer.last_name)='KHOTTCHENKOVA'),
--staff_id 
(SELECT staff.staff_id 
		FROM staff 
		ORDER BY random() 
		LIMIT 1),
--rental_id
(SELECT rental.rental_id 
		FROM rental 
		INNER JOIN inventory 
		ON rental.inventory_id = inventory.inventory_id 
		WHERE EXISTS (SELECT film.film_id 
							FROM film
							WHERE upper(film.title) = 'THE INTOUCHABLES'
							AND inventory.film_id=film.film_id)), 
--amount
9.99,
--payment_date
now()),
--customer_id 
((SELECT customer.customer_id 
		FROM customer
		WHERE upper(customer.last_name)='KHOTTCHENKOVA'),
--staff_id 
(SELECT staff.staff_id 
		FROM staff 
		ORDER BY random() 
		LIMIT 1),
--rental_id
(SELECT rental.rental_id 
		FROM rental 
		INNER JOIN inventory 
		ON rental.inventory_id = inventory.inventory_id 
		WHERE EXISTS (SELECT film.film_id 
							FROM film
							WHERE upper(film.title) = 'KNOCKIN'' ON HEAVEN''S DOOR'
							AND inventory.film_id=film.film_id)), 
--amount
19.99,
--payment_date
now())
RETURNING *;
