--1. Create table �table_to_delete� and fill it with the following query:
CREATE TABLE table_to_delete AS 
							SELECT 'veeeeeeery_long_string' || x AS col 
							FROM   generate_series(1,(10^7)::int) x; --generate_series() creates 10^7 rows of sequential numbers from 1 to 10000000 (10^7)
-- operation time: 51.131 s
							
--2.Lookup how much space this table consumes with the following query:
SELECT *, pg_size_pretty (total_bytes) AS total, --����������� ������ � ������, �������������� � �������� ��������� ����, � �������� �������� ������ � ��������� ���������
			pg_size_pretty (index_bytes) AS INDEX,
			pg_size_pretty (toast_bytes) AS toast,
			pg_size_pretty (table_bytes) AS TABLE
FROM 
	( SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes --����� ������� ��� ����� ���� �������� � ������ TOAST
		FROM 
			(SELECT c.oid, 
				nspname AS table_schema, --��� �����
				relname AS TABLE_NAME,  --��� �������
				c.reltuples AS row_estimate,  --���������� �����
				pg_total_relation_size (c.oid) AS total_bytes,  --����� �����, ������� �������� �� ����� �������� �������, ������� ��� ������� � ������ TOAST
				pg_indexes_size(c.oid) AS index_bytes, --����� ����� ��������, ��������� � ��������� ��������
				pg_total_relation_size (reltoastrelid) AS toast_bytes --����� ����� TOAST
			FROM pg_class c
			LEFT JOIN pg_namespace n
			ON n.oid= c.relnamespace 
			WHERE relkind= 'r') 
		a) 
		a 
		WHERE table_name LIKE '%table_to_delete%';
	--Results:
--      oid     table_schema       table_name     row_estimate         total       index        toast         table 
	-- 17224	public	         table_to_delete	9999884.0		   575 MB	  0 bytes	  8192 bytes	  575 MB
	
--3.Issue the following DELETE operation on �table_to_delete�:
DELETE 
FROM table_to_delete
WHERE REPLACE(col, 'veeeeeeery_long_string','')::int % 3 = 0; --removes 1/3 of all ROWS

--a. Note how much time it takes to perform this DELETE statement:
--1 m 16 s

--b. Lookup how much space this table consumes after previous DELETE:
-- oid     table_schema       table_name      row_estimate      total       index        toast         table 
--17224    	public	        table_to_delete	    9999884.0	   575 MB	    0 bytes	    8192 bytes	   575 MB

--After DELETE the table consumes exectly the same space. In normal PostgreSQL operation, tuples that are deleted or obsoleted by an update are not physically 
--removed from their table. DELETE is MVCC-safe. The record in the table after DELETE is marked as in-valid, while the record itself is not deleted.

--c. Perform the following command (if you're using DBeaver, press Ctrl+Shift+O to observe server output (VACUUM results)):
VACUUM FULL VERBOSE table_to_delete;--vacuuming "public.table_to_delete"

--"table_to_delete": found 0 removable, 6666667 nonremovable row versions in 73530 pages
--30.773 s

--d. Check space consumption of the table once again and make conclusions;
-- oid     table_schema       table_name     row_estimate       total       index        toast         table 
--17224	      public	    table_to_delete	  6666633.0	       383 MB	  0 bytes	   8192 bytes	  383 MB

--VACUUM has made lesser a number of rows and space that table consumes. Only TOAST table size remained unchanged after the VACUUM.
--TOAST table is 8 KB default.

--VACUUM reclaims storage occupied by "dead" tuples. In normal PostgreSQL operation, tuples that are deleted or obsoleted by an update are not physically 
--removed from their table; they remain present until a VACUUM is done. 
--VACUUM FULL rewrites the entire contents of the table into a new disk file with no extra space, allowing unused space to be returned to the operating 
--system. This form is much slower and requires an exclusive lock on each table while it is being processed.

--e. Recreate �table_to_delete� table;
--DROP TABLE table_to_delete;
--CREATE TABLE table_to_delete AS 
							--SELECT 'veeeeeeery_long_string' || x AS col 
							--FROM   generate_series(1,(10^7)::int) x; --generate_series() creates 10^7 rows of sequential numbers from 1 to 10000000 (10^7

-- 4. Issue the following TRUNCATE operation:
TRUNCATE table_to_delete;

--a. Note how much time it takes to perform this TRUNCATE statement.
--158 ms 

--b. Compare with previous results and make conclusion.
--1 m 16 s VS 158 ms: 
--TRUNCATE is much faster than DELETE (for pretty big number of rows)

--TRUNCATE quickly removes all rows from a set of tables. It has the same effect as an unqualified DELETE on each table, but since it does not actually
-- scan the tables it is faster. Furthermore, it reclaims disk space immediately, rather than requiring a subsequent VACUUM operation. 
--This is most useful on large tables.
--The resulting table looks almost identical internally to a newly 'CREATE'ed table. 
--There are no "dead" rows, the indices are empty, and the table statistics are reset.

--c. Check space consumption of the table once again and make conclusions;
-- oid     table_schema       table_name     row_estimate       total       index        toast         table 
--17236	     public	         table_to_delete	0.0           8192 bytes    0 bytes	    8192 bytes	   0 bytes

--The table is empty and its size is 0 (exept TOAST tabel which is 8 Kb default ).


--a. Space consumption of �table_to_delete� table before and after each operation;
--                         oid     table_schema       table_name     row_estimate       total       index        toast         table 
--after creation of table 17224	      public	   table_to_delete	  9999884.0	        575 MB	  0 bytes	  8192 bytes	575 MB
--after DELETE            17224	      public	   table_to_delete	  9999884.0	        575 MB	  0 bytes	  8192 bytes	575 MB
--after VACUUM FULL       17224	      public	   table_to_delete	  6666633.0	        383 MB	  0 bytes	  8192 bytes	383 MB
--after recreation        17236	      public	   table_to_delete	     0.0	      8192bytes	  0 bytes	  8192 bytes	0 bytes
--and TRUNCATE

--b. Duration of each operation (DELETE, TRUNCATE)
--DELETE: -- 1 m 16 s
--TRUNCATE:  158 ms




