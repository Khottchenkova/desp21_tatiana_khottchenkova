--All comedy movies released between 2000 and 2004, alphabetical
--The tables are joind along the path: film-film_category-category
SELECT f.title, com."name", f.release_year 							--selecting OF film title, film TYPE AND RELEASE year
FROM film f															
INNER JOIN 
(
					SELECT film_id, c."name"						--filtering film_id of comedy movies
					FROM film_category fc 
					INNER JOIN category c 	
					ON fc.category_id = c.category_id 
					WHERE upper(c."name") = 'COMEDY'				
)AS com
ON f.film_id = com.film_id
WHERE f.release_year BETWEEN 2000 AND 2004							--filtering OF films WITH RELEASE YEAR BETWEEN 2000 AND 2004
ORDER BY title;														-- ORDERING of film titels alphabetical


--Revenue of every rental store for year 2017 
--(columns: address and address2 as one column, revenue)
--Variant 1:
--This solution is based on the logic according to which DVD copies with films are strictly tied to stores. 
--Then, to obtain a payment-store correspondence, the tables are joind along the path: payment-rental-inventory-store. 
--In this solution castomers and staff members could be related to different stores.
SELECT concat(a.address, a.address2 ) store_address, sum(p.amount) revenue --concatination TO resive address and address2 as one COLUMN FOR a CASE OF allowable nulls
	FROM payment p
	JOIN rental r ON p.rental_id = r.rental_id
	JOIN inventory i ON r.inventory_id = i.inventory_id
	JOIN store s ON i.store_id = s.store_id
	JOIN address a ON s.address_id = a.address_id
	WHERE extract(YEAR FROM p.payment_date)= 2017			--filtering OF payments FOR year 2017 
	GROUP BY a.address, a.address2 									--GROUPING ON stores addresses TO resive revenue FOR EACH store
	ORDER BY revenue DESC;

--Variant 2:
--This solution is based on the logic according to which staff members are strictly tied to stores as it is showen here: https://www.postgresqltutorial.com/postgresql-sample-database/ 
--Then, to obtain a payment-store correspondence, the tables are joind along the path: payment-staff-store.
SELECT concat(a.address, a.address2 ) store_address, sum(p.amount) revenue --concatination TO resive address and address2 as one COLUMN FOR a CASE OF allowable nulls
	FROM payment p
	JOIN staff st ON st.staff_id = p.staff_id
	JOIN store s ON st.store_id = s.store_id
	JOIN address a ON s.address_id = a.address_id
	WHERE extract(YEAR FROM p.payment_date)= 2017							--filtering OF payments FOR year 2017 
	GROUP BY a.address , a.address2 													--GROUPING ON stores addresses TO resive revenue FOR EACH store
	ORDER BY revenue DESC;

--Top-3 actors by number of movies they took part in 
--(columns: first_name, last_name, number_of_movies, sorted by number_of_moviesin descending order)
--The tables are joind along the path: actor-film_actor. There is no need in film title to count number of movies.
SELECT a.first_name , a.last_name , count(*) AS number_of_movies		--counting OF number OF movies FOR EACH actor/actress
FROM actor a 
INNER JOIN film_actor fa
ON a.actor_id =fa.actor_id
GROUP BY a.actor_id														-- grouping on actor IDs to take into account the possibility of the existence of actors with the same firstname and lastname
ORDER BY number_of_movies DESC 											-- ORDERING TO GET descending order
LIMIT (3);																--limiting TO top-3

--Number of comedy, horror and action movies per year 
--(columns: release_year, number_of_action_movies, number_of_horror_movies, number_of_comedy_movies), 
--sorted by release year in descending order
WITH rest AS (SELECT release_year, "name", count(*) num --creating OF cte for resulting TABLE which containes information about a 
				FROM film f 							--number of comedy, horror and action movies per YEAR. The tables are joind along the path:
				JOIN film_category fc 					--film-film_category-category
				ON f.film_id = fc.film_id 
				JOIN category c 
				ON fc.category_id = c.category_id
				WHERE upper(c."name") IN ('ACTION', 'HORROR', 'COMEDY')
				GROUP BY release_year, "name")
SELECT DISTINCT film.release_year, COALESCE (t1.num , 0) number_of_action_movies, COALESCE (t2.num, 0) number_of_horror_movies, COALESCE (t3.num, 0) number_of_comedy_movies --replasing OF NULLS BY 0
FROM film LEFT JOIN										--I use the release_year field FROM film TABLE to take into account all the years mentioned in the DataBase
					(SELECT release_year, num 			--choosing  OF ACTION movies number FROM cte
					FROM rest 
					WHERE upper(rest."name") = 'ACTION') 
					AS t1
			ON film.release_year = t1.release_year 
					LEFT JOIN 							-- LEFT JOIN: there could be years in the database for which films of the sought categories are not defined 
							(SELECT release_year, num 	--choosing  OF HORROR movies number FROM cte
							FROM rest 
							WHERE upper(rest."name") = 'HORROR') 
							AS t2
					ON film.release_year = t2.release_year
							LEFT JOIN 
									(SELECT release_year, num		--choosing  OF COMEDY movies number FROM cte
									FROM rest
									WHERE upper(rest."name") = 'COMEDY') 
									AS t3
							ON film.release_year = t3.release_year
ORDER BY film.release_year DESC;				--ordering by release year in descending ORDER

--Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?
--Variant 1:
--This solution is based on the logic according to which DVD copies with films are strictly tied to stores. 
--Then, to obtain a staff-payment-store correspondence, the tables are joind along the path: staff-payment-rental-inventory-store. 
--In this solution staff members could be related to different stores.
WITH rev AS (SELECT sum(p.amount) revenue, i.store_id store, concat(a.address, a.address2) store_address, concat (st.first_name , ' ', st.last_name) staff_name
   FROM staff st
     JOIN payment p ON st.staff_id = p.staff_id 
     JOIN rental r ON p.rental_id = r.rental_id
     JOIN inventory i ON r.inventory_id = i.inventory_id
     JOIN store s ON i.store_id = s.store_id
     JOIN address a ON s.address_id = a.address_id
     WHERE extract(YEAR FROM p.payment_date)= 2017 					--filtering OF DATA FOR 2017 year
  GROUP BY st.first_name , st.last_name, store_address, i.store_id --GROUPING TO obtaine revenue FOR EACH staff MEMBER OF EACH store. One staff MEMBER could WORK IN different stores.
  )
  SELECT rev.revenue max_revenue, rev.store, rev.store_address, rev.staff_name
  FROM rev
  WHERE  rev.revenue IN (SELECT max(rev.revenue) FROM rev GROUP BY rev.store) --filtering OF maximum VALUES OF revenue FOR EACH store
  ORDER BY max_revenue;

--Variant 2:
--This solution is based on the logic according to which staff members are strictly tied to stores as it is shown here: https://www.postgresqltutorial.com/postgresql-sample-database/ 
--Then, to obtain a staff-payment-store correspondence, the tables are joind along the path: payment-staff-store.
WITH rev AS (SELECT sum(p.amount) revenue, st.store_id store, concat(a.address, a.address2) store_address, concat (st.first_name , ' ', st.last_name) staff_name
   FROM payment p
	JOIN staff st ON st.staff_id = p.staff_id
	JOIN store s ON st.store_id = s.store_id
	JOIN address a ON s.address_id = a.address_id
	WHERE extract(YEAR FROM p.payment_date)= 2017					--filtering OF DATA FOR 2017 year
  GROUP BY st.first_name , st.last_name, store_address, st.store_id --GROUPING TO obtaine revenue FOR EACH staff MEMBER OF EACH store.
  )
  SELECT rev.revenue max_revenue, rev.store, rev.store_address, rev.staff_name
  FROM rev
  WHERE  rev.revenue IN (SELECT max(rev.revenue) FROM rev GROUP BY rev.store) ----filtering OF maximum VALUES OF revenue FOR EACH store
  ORDER BY max_revenue DESC;

--Which 5 movies were rented more than others and what's expected audience age for those movies?	
--In case when number of movies with the same rental-number is more then 5, limiting of top-5 is alphabethical
 WITH top AS (SELECT f.title film_title, f.rating --creating OF cte FOR top-5 film's titels AND their MPA rating
FROM film f										--The tables are joind along the path: film-inventory-rental
INNER JOIN 										
			(SELECT i.film_id , r.rental_date 
			FROM rental r 
			INNER JOIN
			inventory i 
			ON r.inventory_id = i.inventory_id) AS frd
ON f.film_id = frd.film_id
GROUP BY f.title, f.rating
ORDER BY Count(*) DESC, f.title --ORDERING BY the number of times the film was rented FROM maximum AND film title alphabetical
LIMIT (5))
SELECT top.film_title, CASE     --replacing OF rating code BY expected audience
WHEN top.rating = 'G' THEN 'General audiences  All ages admitted.'
WHEN top.rating = 'PG' THEN 'Parental guidance suggested  Some material may not be suitable for children.'
WHEN top.rating = 'PG-13' THEN 'Parents strongly cautioned  Some material may be inappropriate for children under 13.'
WHEN top.rating = 'R' THEN 'Restricted  Under 17 requires accompanying parent or adult guardian.'
WHEN top.rating = 'NC-17' THEN 'Adults Only  No one 17 and under admitted.'
end audience
FROM top;

--Which actors/actresses didn't act for a longer period of time than others?
--The tables are joind along the path: actor-film_actor-film
WITH la AS (SELECT fa.actor_id , MAX(f.release_year) last_acting  --creatin OF cta FOR actor's IDs AND their LAST acting years
			FROM film f 
			INNER JOIN
			film_actor fa 
			ON f.film_id = fa.film_id
			GROUP BY fa.actor_id)
SELECT a.first_name ||' '||a.last_name actor_name, EXTRACT (YEAR FROM CURRENT_DATE)- last_acting "period"  --counting OF the PERIOD OF non-acting
FROM actor a 
INNER JOIN 
la
ON a.actor_id = la.actor_id
WHERE la.last_acting = (SELECT Min(la.last_acting)
					FROM la);  --filtering OF actors/actresses didn't act for a longer period of time than OTHERS (minimum last acting year)
