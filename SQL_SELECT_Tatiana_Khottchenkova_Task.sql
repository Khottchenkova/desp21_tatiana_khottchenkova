--Top-3 most selling movie categories of all time 
--and total dvdrental income for each category.
-- Only consider dvdrental customers from the USA.
--USA as United States in DataBase
WITH rent_usa AS (
				SELECT rental_id --filterin OF rental id FOR customers FROM USA
				FROM rental r 
				INNER JOIN customer c 
				ON r.customer_id = c.customer_id 
				INNER JOIN 
				address a 
				ON a.address_id = c.address_id 
				INNER JOIN 
				city c2 
				ON c2.city_id =a.city_id 
				INNER JOIN 
				country c3 
				ON c3.country_id =c2.country_id 
				WHERE upper (c3.country) ='UNITED STATES'), --changed AFTER checking
				
		cat_usa AS 	(SELECT c."name" , r.rental_id --list OF rents FROM USA ON categories
				FROM rental r 
				INNER JOIN inventory i 
				ON r.inventory_id =i.inventory_id 
				INNER JOIN film_category fc 
				ON fc.film_id = i.film_id 
				INNER JOIN category c 
				ON c.category_id = fc.category_id
				WHERE r.rental_id IN (
										SELECT rent_usa.rental_id
										FROM rent_usa)
				)
SELECT ca."name", count(ca.rental_id) number_of_rents_in_usa, sum(p2.amount) income --counting OF rents per category AND incom per category
FROM payment p2 
INNER JOIN 
cat_usa ca
ON ca.rental_id=p2.rental_id
GROUP BY ca."name"   --GROUPING BY category
ORDER BY income DESC 
LIMIT(3); --top-3 limeting

--For each client, display a list of horrors that he had ever rented 
--(in one column, separated by commas), and the amount of money that he paid for it
WITH hor AS 	(SELECT r.rental_id, f.title 											--list OF horror films WITH rental_id
				FROM rental r 
				INNER JOIN inventory i 
				ON r.inventory_id =i.inventory_id 
				INNER JOIN film_category fc 
				ON fc.film_id = i.film_id 
				INNER JOIN category c 
				ON c.category_id = fc.category_id
				INNER JOIN film f 
				ON f.film_id =fc.film_id 
				WHERE upper(c."name") = 'HORROR')
SELECT c2.first_name ||' '||c2.last_name customer_name, string_agg(DISTINCT hor.title, ', ') movies, sum(p.amount) payment --joining OF movi titles TO one list AND counting the payment FOR horrors
FROM customer c2 
INNER JOIN payment p 
ON c2.customer_id = p.customer_id 
INNER JOIN hor
ON hor.rental_id=p.rental_id --changed AFTER checking
GROUP BY c2.first_name , c2.last_name 								--changed AFTER checking
ORDER BY payment DESC , c2.first_name, c2.last_name desc;  			--orderin BY total payment and then castomer name
				
